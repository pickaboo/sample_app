module UsersHelper
	
	def 	gravatar_for(user, opt = {} )
		gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
		size = opt[:size]
		gravatar_url = "http://secure.gravatar.com/avatar/#{gravatar_id}?size=#{size}"
		image_tag(gravatar_url, alt: "user.name", class: "gravatar")
	end
end
